""
"" Plugin Management - https://github.com/tpope/vim-pathogen
""
execute pathogen#infect()

""
"" Basic Setup
""

set nocompatible      " Remove vi compatibility
set number            " Show line numbers
set ruler             " Show line and column number
set encoding=utf-8    " Set default encoding to UTF-8

""
"" Colour Scheme
""
syntax enable         " Turn on syntax highlighting allowing local overrides
set t_Co=256          " Set 256 colours (needed for gnome-terminal)
colorscheme badwolf   " Set colour scheme

""
"" Whitespace
""

filetype plugin indent on         " Setup auto indent when available
set nowrap                        " don't wrap lines
set tabstop=2                     " a tab is two spaces
set shiftwidth=2                  " an autoindent (with <<) is two spaces
set expandtab                     " use spaces, not tabs
set list                          " Show invisible characters
set backspace=indent,eol,start    " backspace through everything in insert mode

" List chars
set listchars=""                  " Reset the listchars
set listchars=tab:\ \             " a tab should display as "  "
set listchars+=trail:.            " show trailing spaces as dots
set listchars+=extends:>          " The character to show in the last column when wrap is
                                  " off and the line continues beyond the right of the screen
set listchars+=precedes:<         " The character to show in the last column when wrap is
                                  " off and the line continues beyond the left of the screen

""
"" Searching
""

set hlsearch    " highlight matches
set incsearch   " incremental searching
set ignorecase  " searches are case insensitive...
set smartcase   " ... unless they contain at least one capital letter

""
"" Backup and swap files
""

set backupdir^=~/.vim/_backup//    " where to put backup files.
set directory^=~/.vim/_temp//      " where to put swap files.

"" Word wrapping
if exists('+colorcolumn')
  let &colorcolumn="100,".join(range(120,999),",")
endif

"" C++-specific
set cino=N-s  " don't indent inside namespaces
