# Mario's vim Setup

My vim configuration. 

```
  $ cd ~
  $ git clone https://bitbucket.org/mariobadr/vim-setup.git .vim
  $ cd .vim
  $ git submodule update --init
  $ cd ~
  $ ln -s .vim/.vimrc .vimrc
```
